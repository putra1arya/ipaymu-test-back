<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->name(),
            'pekerjaan' => $this->faker->jobTitle(),
            'tanggal_lahir' => $this->faker->date($format = 'Y-m-d', $max = 'now')
        ];
    }
}
