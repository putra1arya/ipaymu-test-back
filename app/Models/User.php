<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];
    public $timestamps = false;

    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }
    public static function boot(){
        parent::boot();
        static::creating(function ($issue) {
            $issue->id = Str::uuid();
            $tgl = explode("-",$issue->tanggal_lahir)[2] % 2;
            if($tgl == 1){
                $ket = "tanggal ganjil";
            }else{
                $ket = "tanggal genap";
            }
            $week = intval(date("W", strtotime($issue->tanggal_lahir))) % 2;
            if($week == 1){
                $ket .= ", minggu ganjil";
            }else{
                $ket .= ", minggu genap";
            }
            $issue->keterangan = $ket;
        });
        self::updating(function($issue){
            $tgl = explode("-",$issue->tanggal_lahir)[2] % 2;
            if($tgl == 1){
                $ket = "tanggal ganjil";
            }else{
                $ket = "tanggal genap";
            }
            $week = intval(date("W", strtotime($issue->tanggal_lahir))) % 2;
            if($week == 1){
                $ket .= ", minggu ganjil";
            }else{
                $ket .= ", minggu genap";
            }
            $issue->keterangan = $ket;
        });
    }

    protected $casts = [
        'keterangan' => 'array'
    ];
}
