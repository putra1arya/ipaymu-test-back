<?php

namespace App\Traits;

trait ResponseTrait{
    public function responseSuccess($success,$message,$data = []){
        return response()->json([
            "success" => $success,
            "message" => $message,
            "data" => $data
        ],200);
    }
    public function responseFailed($success,$message,$status,$data = []){
        return response()->json([
            "success" => $success,
            "message" => $message,
            "data" => $data
        ],$status);
    }
}

?>
