<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Models\User;
use App\Http\Requests\CrudRequest;

class CrudController extends Controller
{
    use ResponseTrait;
    public function show(Request $request){
        $datas = User::where('nama','Like','%'.$request->search.'%')
                ->where('keterangan','LIKE','%'.$request->filter."%")
                ->orderby('nama')
                ->paginate(10);
        return $this->responseSuccess(true,"Berhasil Get Data", $datas);
    }
    public function store(CrudRequest $request){
        User::create([
            'nama' => $request->nama,
            'pekerjaan' => $request->pekerjaan,
            'tanggal_lahir' => $request->tanggal_lahir
        ]);
        return $this->responseSuccess(true,"Berhasil Insert New Data");
    }
    public function update(CrudRequest $request,$id){
        User::find($id)->update([
            'nama' => $request->nama,
            'pekerjaan' => $request->pekerjaan,
            'tanggal_lahir' => $request->tanggal_lahir
        ]);
        return $this->responseSuccess(true,"Berhasil Update Data");
    }
    public function destroy($id){
        try {
            $data = User::findOrFail($id)->delete();
            return $this->responseSuccess(true,"Berhasil Hapus Data");
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->responseFailed(false,"Data Tidak Ditemukan",404);
        }
    }
}
